<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">  

    <title>Enquiry</title>
</head>
<body>
<h1>Enquiry mail</h1>
<!--table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Number</th>
      <th scope="col">Alternate Number</th>
      <th scope="col">E-mail</th>
      <th scope="col">Variant Model</th>
      <th scope="col">Enquiry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{{ $details['name'] }}</th>
      <td>{{ $details['num'] }}</td>
      <td>{{ $details['altNum'] }}</td>
      <td>{{ $details['email'] }}</td>
      <td>{{ $details['var_mod'] }}</td>
      <td>{{ $details['enq'] }}</td>
    </tr>
  </tbody>
</table-->
      <p>Name             : {{ $details['name'] }}</p>
      <p>Number           : {{ $details['num'] }}</p>
      <p>Alternate Number : {{ $details['altNum'] }}</p>
      <p>E-mail           : {{ $details['email'] }}</p>
      <p>Variant Model    : {{ $details['var_mod'] }}</p>
      <p>Enquiry          : {{ $details['enq'] }}</p>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>

