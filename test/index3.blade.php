@include ('header')
@include ('navbar2')

<!-------------------------------------------------------Hero----------------------------------------------------->

 <section id="hero" class="d-flex align-items-center">
  <div class="container">
	<h1 class="ms-3 span-yellow fw-bold">Ideate. Innovate. Integrate.</h1>
    <h2 class="ms-3 span-yellow">Redefining solar energy in India since 1998.</h2>
	<a href="#" class="btn border-0 btn-danger btn-get-started" role="button">View our products</a>
  </div>
 </section>	
     <!------------------------------------------------About us----------------------------------------------->
     <section id="about-us" class="about-us border-bottom">
      <div class="container">

        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>About us</h3>
              <p>
              Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products..
              </p>
              <div class="text-center">
                <a href="#" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
			  <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Our Services</h4>
                    <p>Design<br/>Production<br/>Contract Manufacturing<br/>OEM<br/>Job Contraction</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>Our Vision</h4>
                    <p>To provide the most compelling value in the solar energy industry.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Our Mission</h4>
                    <p>To accelerate the adoption of solar energy systems by providing unparalleled value.</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End of About us -->
       <!-- ======= Why us Section ======= -->
       <section id="why-us" class="why-us">
      <div class="container">

        <div class="section-title">
          <h2>Why us?</h2>
          <p>An ISO 9001:2015 certified company.</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-cogs"></i></div>
              <h4><a href="">Engineering</a></h4>
              <p>Customer requirements and site conditions are considered when designing the system.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-users"></i></div>
              <h4><a href="">Research & Development</a></h4>
              <p>In-house R&D provides faster solutions to meet customer requirements.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-industry"></i></div>
              <h4><a href="">Manufacturing</a></h4>
              <p>ISO 9001:2015 certified production facility.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-microchip"></i></div>
              <h4><a href="">I & C</a></h4>	 
              <p>Highly experienced and dedicated team with engineers for faster and successful execution.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-graduation-cap"></i></div>
              <h4><a href="">Expertise</a></h4>
              <p>Four decades of experience in design and manufacturing.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="fa fa-phone"></i></div>
              <h4><a href="">After Sales Services</a></h4>
              <p>Guaranteed after sales support in various locations.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why us Section -->
<!---------------------------------------------Services Mobile----------------------------------------------->	 
 <!--section class="services-mobile">	
		<div class="container-fluid card-display py-4">
			<div class="card p-2 shadow-sm">
				<div class="row mt-3 mb-2">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center fw-bold">
								Our <span class="span-yellow">Services</span>
							</h1>
						</div>
					</div>
				</div>
				<a href="#" style="text-decoration: none; color: black;">
					<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<span class="card-title">Job Contracting</span>
									<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
								</div>
							</div>
						</div>
					</div>
				</a>
				<div class="card mb-2 " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Contract Manufacturing</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title">OEM</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Production</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="row d-md-none">
					<div class="col-md-12 text-center">
						<a href="#" class="btn btn-danger border-0 hero-button my-3">View All The Services</a>
					</div>
				</div>
			</div>	
		</div>
	</section-->

<!--------------------------------------------------Products Carousel---------------------------------------->	
	<main class="pt-1 pb-3">
	  <div class="container-fluid">
		<div class="row mt-3 mb-3">
			<div class="col-md-12">
				<div class="page-header">
					<h1 class="text-center fw-bold">
						Some Of Our <span class="span-yellow">Products</span>
					</h1>
				</div>
			</div>
		</div>
		<div id="slick">

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		   <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		</div>
        <div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button">View Our Products</a>
				</div>
			</div>
	  </div>
	</main>
<!-------------------------------------------Some of our projects------------------------------------------------>
<section class="projects-customer-cover pt-4">	
	<section>
		<div class="container-fluid pb-3">
		<div class="card p-2 mb-3 projects-card">
			<div class="row">
				<div class="col-md-12 my-2">
					<div class="page-header">
						<h1 class="text-center fw-bold">
							Some Of Our <span class="span-yellow">Projects</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row mt-4 mb-3">
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-1.jpg') }}" class="card-img-top custCard p-1" alt="...">
						<div class="card-body text-center">
							<p class="card-text fw-bold">3.5KW at RR Temple, Bangalore through BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-4.jpg') }}" class="card-img-top custCard p-1">
						<div class="card-body text-center">
							<p class="card-text fw-bold">42KW at ESD, BHEL, Bangalore thorough BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-6.jpg') }}" class="card-img-top custCard p-1">
						<div class="card-body text-center">
							<p class="card-text fw-bold">40KW at Engineering College, Kanchipuram through BHEL</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button">View All Of Our Projects</a>
				</div>
			</div>
		</div>	
		</div>
	</section>
<!---------------------------------------------------Customer Testimonials------------------------------------>	
	<section class="customer-test-cover">
		<div class="container-fluid d-none d-md-block">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="page-header">
						<h1 class="text-center fw-bold text-white">
						Customer Testimonials
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bhel.gif') }}" class="card-title">
							<p class="card-text">“In a single contact, we received detailed and timely assistance. For improved quality,
							I recommend this company and its professional engineers.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/reliance-solar-group.gif') }}" class="card-title">
							<p class="card-text">“I always say that the quality and service at this organisation are fantastic, and this is just another example of the positive work you all do there.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bharat-electronics.gif') }}" class="card-title">
							<p class="card-text">“This team provides exceptional service and assistance both before and after the purchase.”</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button my-3">Read More Customer Testimonials</a>
				</div>
			</div>
		</div>
	</section>
</section>	


@include ('footerbar')
@include ('footer')

<script>
var breakpoint = {
  // Small screen / phone
  sm: 576,
  // Medium screen / tablet
  md: 768,
  // Large screen / desktop
  lg: 992,
  // Extra large screen / wide desktop
  xl: 1200
};

// page slider
$('#slick').slick({
	autoplay: true,
  autoplaySpeed: 0,
  draggable: true,
  infinite: true,
  dots: false,
  arrows: false,
  speed: 5000,
  mobileFirst: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: breakpoint.sm,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: breakpoint.md,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: breakpoint.lg,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: breakpoint.xl,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5
      }
    }
  ]
});

</script>	

