@include ('header') @include ('navbar')
<!-- Enquiry Modal -->

<div
    class="modal fade"
    id="EnquiryModal"
    tabindex="-1"
    aria-labelledby="EnquiryModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EnquiryModalLabel"></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
                <form method="post" id="enqForm">
                    @csrf
                    <div
                        class="input-group visually-hidden"
                        id="hide_inp"
                    ></div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1"
                            >Name</span
                        >
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Enter Name"
                            aria-label="Name"
                            aria-describedby="basic-addon1"
                            id="name"
                            name="name"
                            autofocus
                            required
                        />
                        <span class="text-danger error-text name_err"></span>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon2"
                            >Number</span
                        >
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Enter Number"
                            aria-label="Number"
                            aria-describedby="basic-addon2"
                            id="num"
                            name="number"
                            required
                        />
                        <span class="text-danger error-text num_err"></span>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon4"
                            >Alternate Number</span
                        >
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Enter Alternate Number"
                            aria-label="AlternateNumber"
                            aria-describedby="basic-addon4"
                            id="altNum"
                            name="altNumber"
                            required
                        />
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon3"
                            >E-mail</span
                        >
                        <input
                            type="mail"
                            class="form-control"
                            placeholder="Enter E-mail"
                            aria-label="E-mail"
                            aria-describedby="basic-addon3"
                            id="email"
                            name="mail"
                            required
                        />
                        <span class="text-danger error-text email_err"></span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-text">Enquiry</span>
                        <textarea
                            class="form-control"
                            aria-label="With textarea"
                            id="enq"
                            name="enquiry"
                            required
                        ></textarea>
                        <span class="text-danger error-text enq_err"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit" id="formSubmit">
                    Send via Email
                </button>
                <button
                    class="btn btn-success"
                    type="submit"
                    href="#"
                    target="_blank"
                    id="wp-button"
                >
                    Send via Whatsapp
                </button>
            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------Hero---------------------------------------------------------->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 pt-3 shadow" id="cat_bar">
            <h2 class="ms-3 mt-3 fw-bold fs-4">Filter by categories</h2>
            <ul class="nav flex-column" id="category-list">
            <li class='nav-item selected'>
                    <a  class='nav-link fw-bold catLink text-white' href='#'> oddular </a>
            </li>
            <li class='nav-item hlt'>
                    <a  class='nav-link fw-bold catLink' href='#'> adad </a>
            </li>
            </ul>
        </div>
        <div class="col-md-9 pt-3 pe-4" id="pro_bar">
            <h3 id="catName" class="mt-3 fw-bold fs-4">
                Solar Charge Controllers - PWM<br />CHARGE CONTROLLER 12V DC 5A
                CHARGING,5A LOAD - MSCC1205E
            </h3>
            <div class="row">
                <div class="col-md-6">
                    <img
                        src="images/charge-c-01.jpg"
                        alt=""
                        class="img-fluid"
                    />
                </div>
                <div class="col-md-6 grid-center">
                    <div class="d-grid gap-2 col-6" id="proBtn">
                        <a
                            class="btn btn-primary"
                            href="pdf/User Manual charge controller_12_24V_5_10_15A.pdf"
                            target="blank"
                            >User Manual</a
                        >
                        <a
                            class="btn btn-primary"
                            href="pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf"
                            target="blank"
                            >Data Sheet</a
                        >
                        <a
                            class="btn btn-primary"
                            id="enq-btn"
                            data-bs-toggle="modal"
                            data-bs-target="#EnquiryModal"
                            data-bs-dismiss="modal"
                            >Make an enquiry for this product</a
                        >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button
                                class="accordion-button"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseOne"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                            >
                                Description
                            </button>
                        </h2>
                        <div
                            id="collapseOne"
                            class="accordion-collapse collapse show"
                            aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                                <p>
                                    The Charge Controller of Maruthi Solar
                                    product lines are the best option for
                                    compact, cost effective stand-alone solar
                                    systems. This charge controller’s
                                    centerpiece is a microprocessor/IC, which
                                    controls the security and information
                                    functions as well as guaranteeing optimal
                                    charge control. Maruthi Solar charge
                                    controller can also charge flooded /sealed
                                    lead acid/Lithium batteries. In order to
                                    maximize the battery’s life further, the
                                    charge controllers also feature integrated
                                    temperature compensation. The housing can be
                                    installed directly onto a wall.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo"
                                aria-expanded="false"
                                aria-controls="collapseTwo"
                            >
                                Technical Specification
                            </button>
                        </h2>
                        <div
                            id="collapseTwo"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                                <img
                                    src="images/techSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseThree"
                                aria-expanded="false"
                                aria-controls="collapseThree"
                            >
                                Mechanical Specifications
                            </button>
                        </h2>
                        <div
                            id="collapseThree"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                                <img
                                    src="images/mechSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // $('a.catLink').on('click', function (event){
        //     // alert("click");
        //     // console.log("click");
        //     //$('li.hlt').addClass( "selected" );
        //     // $('a.catLink').addClass( "text-white" );
        //     $target = $(event.target);   
        //     $target.addClass('selected');
        // });
        $('a.catLink').click(function(e) {

        $('li.selected').removeClass('selected');
        $('a.text-white').removeClass('text-white');

        var $parent = $(this).parent();
        var $child = $(this).parent().children();
        $parent.addClass('selected');
        $child.addClass('text-white');
        e.preventDefault();
        });
    });
</script>
@include ('footerbar')@include ('footer')
