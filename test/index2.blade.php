@include ('header')
@include ('navbar')

<!-------------------------------------------------------Hero----------------------------------------------------->
	<section class="border-bottom">
		<div class="container-fluid cover">
			<div class="row h-100">
				<div class="col-md-7 col-xs-12 d-flex align-items-center">
					<div class="">
						<div class="row power-margin mobile-align mb-3">
							<div class="col-md-12">
								<h1 class="ms-3 span-yellow fw-bold">
									Ideate. Innovate. Integrate.
								</h1>
							</div>
						</div>
						<div class="row mobile-align mb-3">
							<div class="col-md-12">
								<h5 class="ms-3 span-yellow">
									Redefining solar energy in India since 1998.
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 mt-2 mb-3">
								<div class="d-grid gap-2 d-md-block btn-group ms-md-3 btn-margin">
								  <!--a href="#" class="btn btn-primary border-0 hero-button" role="button">Get in touch with us!</a-->
								  <a href="#" class="btn border-0 btn-danger hero-button shadow" role="button">View our products</a>
								  <!--a href="#" class="btn btn-primary border-0 hero-button d-md-none" role="button">Services</a-->
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-md-5 h-100 d-none d-sm-block">
					<div class="my-5 mx-3">
						<div class="row">
							<div class="col-md-6" >				
								<div class="card mb-3 custCard">
									<img src="{{ asset('images/jobc.jpg') }}" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Job Contracting
										</span>
									</div>	
								</div>
							</div>
							<div class="col-md-6" >
								<div class="card custCard mb-3">
									<img src="{{ asset('images/card3.jpg') }}" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Contract Manufacturing
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="card custCard mb-3">
									<img src="{{ asset('images/card2.jpg') }}" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										OEM
										</span>
									</div>
								</div>
							</div>					
							<div class="col-md-6">
								<div class="card custCard">
									<img src="{{ asset('images/card1.jpg') }}" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Production
										</span>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>	
     <!-- ======= Why Us Section ======= -->
     <section id="why-us" class="why-us border-bottom">
      <div class="container py-3">

        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>About us</h3>
              <p>
              Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products..
              </p>
              <div class="text-center">
                <a href="#" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>Our Vision</h4>
                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Our Mission</h4>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Our Services</h4>
                    <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->
<!---------------------------------------------Services Mobile----------------------------------------------->	 
 <section class="services-mobile">	
		<div class="container-fluid card-display py-4">
			<div class="card p-2 shadow-sm">
				<div class="row mt-3 mb-2">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center fw-bold">
								Our <span class="span-yellow">Services</span>
							</h1>
						</div>
					</div>
				</div>
				<a href="#" style="text-decoration: none; color: black;">
					<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<span class="card-title">Job Contracting</span>
									<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
								</div>
							</div>
						</div>
					</div>
				</a>
				<div class="card mb-2 " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Contract Manufacturing</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title">OEM</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Production</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="row d-md-none">
					<div class="col-md-12 text-center">
						<a href="#" class="btn btn-danger border-0 hero-button my-3">View All The Services</a>
					</div>
				</div>
			</div>	
		</div>
	</section>
<!------------------------------------------------About us----------------------------------------------->
<section class="about-why-cover pt-2">
	<section>
		<div class="container " >
			<div class="card p-3 mt-3 mb-2 aboutus-card">
				<div class="row mt-2 mb-2">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center fw-bold">
								About <span class="span-yellow">Us</span>
							</h1>
						</div>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12 col-sm-12">
						<p class="fw-bold text-center">
                        Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 flexing ps-0">
						<a href="#" class="btn btn-dark border-0 hero-button" role="button">Read more</a>
					</div>
				</div>
			</div>		
				<div class="row  d-sm-none d-md-inline d-flex justify-content-between">
					<div class="col-md-4 mb-3  d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto custCard">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Vision</span></h5>
								<p class="card-text mt-3">To be a worldwide solar product provider to meet the global energy needs.</p>
							</div>	
						</div>
					</div>
					<div class="col-md-4 mb-3 d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto custCard">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Mission</span></h5>
								<p class="card-text mt-3">To Provide cost effective & reliable solar energy solutions.</p>
							</div>	
						</div>
					</div>
					<div class="col-md-4 mb-3 d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto custCard">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Services</span></h5>
								<p class="card-text">Design<br/>Production<br/>Contract Manufacturing<br/>OEM<br/>Job Contraction</p>
							</div>	
						</div>
					</div>
				</div>
		</div>
	</section>
	<section class="why-us-cover">
		<div class="container-fluid py-3">
			<div class="row">
			<div class="col-md-12 flexing">
			<div class="row h-100">
				<div class="col-md-6 flexing d-none d-sm-block">
					<div class="card card-team-us">
						<div class="card-body text-center">
							<h5 class="card-title mt-1 fw-bold">Our <span class="span-yellow">Team</span></h5>
							<p class="card-text">The company is managed by technically qualified, professionally experienced, Young and Dynamic Technocrats.The in- house Research and Development, helps in developing the products of high quality and reliability with optimum design and considerable reduction in cost. The quality being the most important parameter and with guaranteed after sales service...
							</p>
							<a href="#" class="btn btn-dark border-0 hero-button mt-4">Read more</a>
						</div>	
					</div>
				</div>
				<div class="col-md-6 flexing">
					<div class="card card-team-us">
						<div class="card-body text-center">
							<h5 class="card-title mobile-text-bold fw-bold">Why <span class="span-yellow">Us?</span></h5>
							<p class="card-text">We have established a strong foothold in the domain due to the superior nature of the product range offered & our expertise in the solar industries for more than two decades.
							Our goods are in line with industry requirements and are available at the most reasonable prices. We select specialists who work long hours to manufacture goods according to the standards and provided by... </p>
							<a href="#" class="btn btn-dark border-0 hero-button mt-4">Read more</a>
						</div>	
					</div>
				</div>
			</div>
			</div>
			</div>
		</div>
	</section>
</section>	

<!--------------------------------------------------Products Carousel---------------------------------------->	
	<main class="pt-1 pb-3">
	  <div class="container-fluid">
		<div class="row mt-3 mb-3">
			<div class="col-md-12">
				<div class="page-header">
					<h1 class="text-center fw-bold">
						Some Of Our <span class="span-yellow">Products</span>
					</h1>
				</div>
			</div>
		</div>
		<div id="slick">

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		   <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		</div>
        <div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button">View Our Products</a>
				</div>
			</div>
	  </div>
	</main>
<!-------------------------------------------Some of our projects------------------------------------------------>
<section class="projects-customer-cover pt-4">	
	<section>
		<div class="container-fluid pb-3">
		<div class="card p-2 mb-3 projects-card">
			<div class="row">
				<div class="col-md-12 my-2">
					<div class="page-header">
						<h1 class="text-center fw-bold">
							Some Of Our <span class="span-yellow">Projects</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row mt-4 mb-3">
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-1.jpg') }}" class="card-img-top custCard p-1" alt="...">
						<div class="card-body text-center">
							<p class="card-text fw-bold">3.5KW at RR Temple, Bangalore through BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-4.jpg') }}" class="card-img-top custCard p-1">
						<div class="card-body text-center">
							<p class="card-text fw-bold">42KW at ESD, BHEL, Bangalore thorough BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="{{ asset('images/power-plant-6.jpg') }}" class="card-img-top custCard p-1">
						<div class="card-body text-center">
							<p class="card-text fw-bold">40KW at Engineering College, Kanchipuram through BHEL</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button">View All Of Our Projects</a>
				</div>
			</div>
		</div>	
		</div>
	</section>
<!---------------------------------------------------Customer Testimonials------------------------------------>	
	<section class="customer-test-cover">
		<div class="container-fluid d-none d-md-block">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="page-header">
						<h1 class="text-center fw-bold text-white">
						Customer Testimonials
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bhel.gif') }}" class="card-title">
							<p class="card-text">“In a single contact, we received detailed and timely assistance. For improved quality,
							I recommend this company and its professional engineers.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/reliance-solar-group.gif') }}" class="card-title">
							<p class="card-text">“I always say that the quality and service at this organisation are fantastic, and this is just another example of the positive work you all do there.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bharat-electronics.gif') }}" class="card-title">
							<p class="card-text">“This team provides exceptional service and assistance both before and after the purchase.”</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-dark border-0 hero-button my-3">Read More Customer Testimonials</a>
				</div>
			</div>
		</div>
	</section>
</section>	


@include ('footerbar')
@include ('footer')

<script>
var breakpoint = {
  // Small screen / phone
  sm: 576,
  // Medium screen / tablet
  md: 768,
  // Large screen / desktop
  lg: 992,
  // Extra large screen / wide desktop
  xl: 1200
};

// page slider
$('#slick').slick({
	autoplay: true,
  autoplaySpeed: 0,
  draggable: true,
  infinite: true,
  dots: false,
  arrows: false,
  speed: 5000,
  mobileFirst: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: breakpoint.sm,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: breakpoint.md,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: breakpoint.lg,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: breakpoint.xl,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5
      }
    }
  ]
});

</script>	

