@include ('header')
<form id="enqForm"> 
		@csrf
		  <div class="input-group visually-hidden" id="hide_inp">
			
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon1">Name</span>
			  <input type="text" class="form-control" placeholder="Enter Name" aria-label="Name" aria-describedby="basic-addon1" id="name" name="name" autofocus required/>
			  <span class="text-danger error-text name_err"></span>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon2">Number</span>
			  <input type="tel" class="form-control" placeholder="Enter Number" aria-label="Number" aria-describedby="basic-addon2" id="num" name="number" maxlength="12" required/>
			  <span class="text-danger error-text num_err"></span>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon4">Alternate Number</span>
			  <input type="tel" class="form-control" placeholder="Enter Alternate Number" aria-label="AlternateNumber" aria-describedby="basic-addon4" id="altNum" name="altNumber" maxlength="12" required/>
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon3">E-mail</span>
			  <input type="mail" class="form-control" placeholder="Enter E-mail" aria-label="E-mail" aria-describedby="basic-addon3" id="email" name="email" required/>
			  <span class="text-danger error-text email_err"></span>
			  
		 </div>
		 <div class="input-group">
			  <span class="input-group-text">Enquiry</span>
			  <textarea class="form-control" aria-label="With textarea" id="enq" name="enquiry" maxlength="500"  required></textarea>
			  <span class="text-danger error-text enq_err"></span>
		 </div>
         <button class="btn btn-primary" type="submit" id="formSubmit">Send via Email</button>
         <!--button class="btn btn-success" type="submit" href="#" target='_blank' id="wp-button">Send via Whatsapp</button-->
</form>
<script>
    $(document).ready(function(){
        
        $.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});

  $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
		});	
		
  $.validator.addMethod("phonenu", function (value, element) {
		return this.optional(element) || /((\+*)((0[ -]+)*|(91 )*)(\d{12}|\d{10}))|\d{5}([- ]*)\d{6}/g.test(value);
    }, "Invalid phone number");	

  $.validator.addMethod("validate_email", function(value, element) {

		if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
			return true;
		} else {
			return false;
		}
		}, "Please enter a valid Email.");

  $("#enqForm").validate({
    rules: {
        name: {
			required : true,
			alpha : true
		},
        number: {
			required: true,
			phonenu: true,
		},
		altNumber: {
			required : false,
			phonenu: true
		},
      	email: {
			required : true,
			validate_email :true
		},
    },
    messages: {
      name: "Please enter a valid name",
      number: "Please enter a valid number",
      email: "Please enter a valid email address"
    },
	submitHandler: function(form){
		
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq		=	$('#enq').val();
				//console.log(enq);
			var vr_id	= $('#var_id').val();
				//console.log(vr_id);
			var var_mod	= $('#var_mod').val();
				//console.log(var_mod);
				
			var formData = {'name':name,'num':num,'altNum':altNum,'email':email,'enq':enq,'vr_id':vr_id, 'var_mod':var_mod};
			//console.log("-----------------------------------------");	
          //console.log(formData);
//DB          
		  $.ajax({
				  type		:'POST',
				  url		:'submit-form-db',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					alert("Success");
				  },
				  error: function(f){
						alert('Enquiry form DB AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		    });
//Email
          $.ajax({
				  type		:'POST',
				  url		:'submit-form-email',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					  alert("Success");
				  },
				  error: function(f){
						alert('Enquiry form Email AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		  });
          return false;
  },
  });

});
</script>
@include('footer')