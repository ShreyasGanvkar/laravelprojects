@include ('header')
@include ('navbar')
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
	<div class="container-fluid products-cover">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold ms-1 mt-3">
				Products
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!------------------------------------------------------------------------------------------------------------>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<h2 class="fs-5 mt-3">Categories</h2>
				<ul class="nav flex-column" id="category-list">

				</ul>
			</div>
			<div class="col-md-9" id="rowCat-list">
				<!--div class="row  row-cols-1 row-cols-md-3 g-4" id="subCategory-list" style="display:none">
			
				</div-->
				<!--div class="row">
					
				</div-->
			</div>
		</div>
	</div>
</section>
<script>
$(document).ready(function(){
	$.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});
	//for all the categories in the vertical navbar
	/*$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var catList = '';
					$.each(response, function(i, category)
						{ 	
							catList += "<li class='nav-item'><a class='nav-link ps-0' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(" +category.PK_cy_id+ ")' > "+category.cy_name+ "</a></li>";
						});
					//console.log(catList);	
					$('#category-list').append(catList);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
		}); */
		
	$.ajax({
		type:"POST",
		url: "get-categories",
		data: {},
        dataType: "json",                    
        cache: false,                       
        success: function(response) 
			{	
				var rowCat = '';
				var storeCatID = '';
				$.each(response, function(i, category)
					{ 
						rowCat += "<div class='row'><div class='row'><div class='col-md-12'><h2>"+category.cy_name+"</h2></div></div><div class='row row-cols-1 row-cols-md-3 g-4' id='subCategory-list"+category.PK_cy_id+"'></div></div>"
							
						storeCatID=category.PK_cy_id;
							//console.log(storeCatID);
						runAjax(storeCatID);
					});
					$('#rowCat-list').append(rowCat);
			},
			error: function(e)
				{
					alert('Outer AJAX Error!');
					console.log('Outer AJAX Error!');
					console.log(e);
				},
							
	});
	
function runAjax(catID){
			
		var sendData = {'cy_id':catID};
				$.ajax({
					type:"POST",
					url: "get-spe-subCategories",
					data: sendData,
					dataType: "json",                    
					cache: false,                       
					success: function(response) 
						{	
							var allsubCategory = '';
							var id= catID;
							//var test='';
							
							//console.log('00000000000000000');
							//console.log(foo.FK_cy_id);
							$.each(response, function(i, subCategory)
								{
									//test = subCategory.FK_cy_id;
									//console.log(test);
									
									allsubCategory += "<div class='col'><div class='card h-100 text-center'><img src='" +subCategory.sub_img+
									"' class='card-img-top' alt='" +subCategory.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCategory.sub_name+ "</h6></div></div></div>"
									
								});
								$('#subCategory-list'+id).append(allsubCategory);
						},
						error: function(e)
							{
								alert('Inner AJAX Error!');
								console.log('Inner AJAX Error!');
								console.log(e);
							},				
				});
		};
		
		
		
});

function retrieveSubCat(catID){
	//var test = catID;
	//console.log(test);
	//$('#subCategory-list').show();
	var sendData = {'cy_id': catID};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+")'><div class='card h-100 text-center'><img src='" +subCat.sub_img+
							"' class='card-img-top' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};


	//for all the subCategory cards
/*	$.ajax({
			type:"POST",
			url: "get-subCategories",
			data: {},
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var allsubCategory = '';
					$.each(response, function(i, subCategory)
						{
							allsubCategory += "<div class='col'><a href='#' onclick='showProd("+subCategory.PK_sub_id+")' ><div class='card h-100 text-center'><img src='" +subCategory.sub_img+
							"' class='card-img-top' alt='" +subCategory.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCategory.sub_name+ "</h6></div></div></a></div>"
						});
					//console.log(catList);	
					$('#subCategory-list').append(allsubCategory);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},				
		});
		
});*/
	
/*function showProd(subID){
	
				var sendData = {'sub_id': subID};
	$.ajax({
			type: "POST",
			url: "get-subcategory-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var subProducts = '';
				$.each(response, function(i, product)
					{ 	
						subProducts += "<div class='col'><a href='#' onclick='showVar("+product.PK_pr_id+")'><div class='card h-100 text-center'><img src='" +product.pr_img+
						"' class='card-img-top' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title'>" +product.pr_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(subProducts);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

function retrieveProducts(catID){
	
	var sendData = {'cy_id': catID};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+")'><div class='card h-100 text-center'><img src='" +subCat.sub_img+
							"' class='card-img-top' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

/*function showVar(prId)
{
	var sendData = {'pr_id': prId};
	$.ajax({
			type: "POST",
			url: "",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				$.each(response, function(i, varProd)
					{ 	
						//prodVar += ;
					});
				//console.log(allProducts);
				$('#subCategory-list').html(prodVar);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});	
};*/
</script>
@include ('footer')
