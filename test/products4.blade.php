@include ('header')
@include ('navbar')
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
	<div class="container-fluid products-cover shadow">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold ms-1 mt-3">
				Products
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!------------------------------------------------------------------------------------------------------------>

<!--Variants Modal-->
	<div class="modal fade" id="varModal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="ModalLabel"></h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		  </div>
		  <div class="modal-body">
			<div class="container-fluid">
				<div class="row">
					<div class="row">
						<div class="col-md-3" id="modalImage">
							
						</div>
						<div class="col-md-9">
							<div class="d-grid gap-2" id="proBtn">

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" id='modalDesc'>
							
						</div>
					</div>
				</div>
				<div class="row">
					<table class='table'>
						<thead>
							<tr>
								<th scope='col'>Model No.</th>
								<th scope='col'>Details</th>
								<th scope='col'>Enquire</th>
							</tr>
						</thead>
						<tbody id="bodyTable">
						
						</tbody>
					</table>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			
		  </div>
		</div>
	  </div>
	</div>
	
<!-- Enquiry Modal -->

<div class="modal fade" id="EnquiryModal" tabindex="-1" aria-labelledby="EnquiryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EnquiryModalLabel">
		
		</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        
		<form>
		  <div class="input-group visually-hidden" id="hide_inp">
			
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon1">Name</span>
			  <input type="text" class="form-control" placeholder="Enter Name" aria-label="Name" aria-describedby="basic-addon1" id="name">
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon2">Number</span>
			  <input type="text" class="form-control" placeholder="Enter Number" aria-label="Number" aria-describedby="basic-addon2" id="num">
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon4">Alternate Number</span>
			  <input type="text" class="form-control" placeholder="Enter Alternate Number" aria-label="AlternateNumber" aria-describedby="basic-addon4" id="altNum">
		 </div>
		 <div class="input-group mb-3">
			  <span class="input-group-text" id="basic-addon3">E-mail</span>
			  <input type="text" class="form-control" placeholder="Enter E-mail" aria-label="E-mail" aria-describedby="basic-addon3" id="email">
		 </div>
		 <div class="input-group">
			  <span class="input-group-text">Enquiry</span>
			  <textarea class="form-control" aria-label="With textarea" id="enq"></textarea>
		 </div>
		  
		</form>
		
      </div>
      <div class="modal-footer">
		<a class="btn btn-primary" href="#"  role="button" id="formSubmit">Send</a>
      </div>
    </div>
  </div>
</div>
	<!---->
	<aside>
	<div class="container-fluid pt-4" >
		<div class="row ps-2">
			<div class="col-md-3" id="cat_bar">
				<h2 class="fs-5 mt-3 fw-bold fs-4">Filter by categories</h2>
				<ul class="nav flex-column" id="category-list">

				</ul>
			</div>
			<div class="col-md-9">
				<h1 ></h1>
				<div class="row  row-cols-1 row-cols-md-3 g-4" id="subCategory-list">
			
				</div>
			</div>
		</div>
	</div>
</aside>
<script>
$(document).ready(function(){
	$.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});
//Displaying all the categories in the vertical navbar
	$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var catList = '';
					var catName='';
					$.each(response, function(i, category)
						{ 	
							catList += "<li class='nav-item'><a class='nav-link ps-0 fw-bold' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(" +category.PK_cy_id+ "); return false' > "+category.cy_name+ "</a></li>";

						});
					//console.log(catList);	
					$('#category-list').append(catList);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
		});
	
//To display subcategories of the first category when the products page is opened for the first time.
	var sendData = {'cy_id': 1};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				var subCatName = '';

				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+"); return false'><div class='card '><img src='" +subCat.sub_img+
							"' class='card-img-top' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"

					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
	
	$('#formSubmit').on('click',(function(){
          //e.preventDefault();
		 
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq		=	$('#enq').val();
				//console.log(enq);
			var vr_id	= $('#var_id').val();
				//console.log(vr_id);
			var var_mod	= $('#var_mod').val();
				//console.log(var_mod);	
				
			var formData = {'name':name,'num':num,'altNum':altNum,'email':email,'enq':enq,'vr_id':vr_id, 'var_mod':var_mod};
				//console.log(formData);
		  $.ajax({
				  type		:'POST',
				  url		:'submit-form-db',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					  alert("Success");
				  },
				  error: function(f){
						alert('Enquiry form DB AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		  });
	  }));

	  $('#formSubmit').on('click',(function(){
          //e.preventDefault();
		 
			var name	=	$('#name').val();	
				//console.log(name);
	    	var num		=	$('#num').val();
				//console.log(num);
			var altNum	=	$('#altNum').val();
				//console.log(altNum);
			var email	=	$('#email').val();
				//console.log(email);
			var enq	=	$('#enq').val();
				//console.log(enq);
			var vr_id = $('#var_id').val();
				//console.log(vr_id);
			var var_mod= $('#var_mod').val();
				//console.log(var_mod);	

			var formData = {'name':name,'num':num,'altNum':altNum,'email':email,'enq':enq,'vr_id':vr_id, 'var_mod':var_mod};
				//console.log(formData);
		  $.ajax({
				  type		:'POST',
				  url		:'submit-form-email',
				  data		:formData,
				  dataType	:'json',                    
				  cache		:false,                       
				  success	:function(response) {
					  alert("Success");
				  },
				  error: function(f){
						alert('Enquiry form Email AJAX Error!');
						console.log('AJAX Error!');
						console.log(f);
					},		
		  });
	  }));
});

//To display sub categories when user selects a category.
function retrieveSubCat(catID){
	//var test = catID;
	//console.log(test);
	var sendData = {'cy_id': catID};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+"); return false'><div class='card h-100 text-center'><img src='" +subCat.sub_img+
							"' class='card-img-top pro-img' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display products when user selects a sub category
function showProd(subID){
	
	var sendData = {'sub_id': subID};
	$.ajax({
			type: "POST",
			url: "get-subcategory-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var subProducts = '';
				$.each(response, function(i, product)
					{ 	
						subProducts += "<div class='col'><a href='#' data-bs-toggle='modal' data-bs-target='#varModal' onclick='varModal("+product.PK_pr_id+")'><div class='card h-100 text-center'><img src='" +product.pr_img+
						"' class='card-img-top pro-img' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title'>" +product.pr_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(subProducts);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display product variant details in the modal when user clicks on a product.

function varModal(prID)
{
	var sendData = {'sub_id': prID};
	$.ajax({
			type: "POST",
			url: "get-product-var",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				$.each(response, function(i, variant)
					{ 	
						prodVar += "<tr><td>"+variant.var_mod_no+"</td><td>"+variant.var_details+"</td><td><a href='#' data-bs-toggle='modal' data-bs-target='#EnquiryModal' data-bs-dismiss='modal' onclick='enqModal("+variant.PK_var_id+")'>Enquire</a></td></tr>"
						
						
					});
				//console.log(allProducts);
				$('#bodyTable').html(prodVar);
				//$('#ModalLabel').html();
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
			
	$.ajax({
			type: "POST",
			url: "get-prod-modal",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var imagesModal = '';
				var descModal = '';
				var proBtn = '';

				$.each(response, function(i, product)
					{ 	
						imagesModal += "<img src='"+product.pr_img+"' class='img-fluid'>";

						proBtn += "<a class='btn btn-primary' target='_blank' href='"+product.pr_man+"'>User Manual</a><a class='btn btn-primary' target='_blank' href='"+product.pr_ds+"'>Data Sheet</a><a class='btn btn-primary' target='_blank' href='"+product.pr_broch+"'>Brochure</a>";

						descModal += "<p>"+product.pr_desc+"</p>";
					});
				//console.log(allProducts);
				$('#modalImage').html(imagesModal);
				$('#proBtn').html(proBtn);
				$('#modalDesc').html(descModal);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},		
			});			
			
			
}

function enqModal(vrID)
{
	var sendData ={'vr_id': vrID};
	$.ajax({
			type: "POST",
			url: "get-var-mod",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				var hide_inp='';
				$.each(response, function(i, variant)
					{ 	
						prodVar += "<span>"+variant.var_mod_no+" - </span><span>"+variant.var_details+"</span>"
						hide_inp += "<input type='text' class='form-control' id='var_id' value='"+variant.PK_var_id+"' disabled><input type='text' class='form-control' id='var_mod' value='"+variant.var_mod_no+"-"+variant.var_details+"' disabled>"
					});
				//console.log(allProducts);
				$('#EnquiryModalLabel').html(prodVar);
				$('#hide_inp').html(hide_inp);
				//$('#ModalLabel').html();
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
	
}
</script>
@include ('footerbar')
@include ('footer')
