@include ('header') @include ('navbar')
<!-- Enquiry Modal -->

<div
    class="modal fade"
    id="EnquiryModal"
    tabindex="-1"
    aria-labelledby="EnquiryModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="EnquiryModalLabel">
                    <!-- Variant details -->Enquiry for<br/>
                </h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
                <form method="post" id="enqForm">
                    @csrf
                    <div class="input-group visually-hidden" id="hide_inp">
                        <!-- Input field with variant model number and name -->
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control border-primary"
                            placeholder="Enter Name"
                            aria-label="Name"
                            aria-describedby="basic-addon1"
                            id="name"
                            name="name"
                            autofocus
                            required
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control border-primary"
                            placeholder="Enter Number"
                            aria-label="Number"
                            aria-describedby="basic-addon2"
                            id="num"
                            name="number"
                            required
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control border-primary"
                            placeholder="Enter Alternate Number"
                            aria-label="AlternateNumber"
                            aria-describedby="basic-addon4"
                            id="altNum"
                            name="altNumber"
                            required
                        />
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="mail"
                            class="form-control border-primary"
                            placeholder="Enter E-mail"
                            aria-label="E-mail"
                            aria-describedby="basic-addon3"
                            id="email"
                            name="mail"
                            required
                        />
                    </div>
                    <div class="input-group">
                        <textarea
                            class="form-control border-primary"
                            aria-label="With textarea"
                            id="enq"
                            name="enquiry"
                            required
                        ></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
             <div class="d-grid gap-2 col-6  mx-auto">
                <button class="btn btn-primary" type="submit" id="formSubmit">
                    Send via Email
                </button>
                <button
                    class="btn btn-success d-block d-sm-none"
                    type="submit"
                    href="#"
                    target="_blank"
                    id="wp-button"
                >
                    Send via Whatsapp
                </button>
              </div>  
            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
    <div class="container-fluid products-cover shadow">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-white fw-bold ms-1 mt-3">Products</h1>
            </div>
        </div>
    </div>
</section>
<!------------------------------------------------------------------------------------------------------------>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 pt-3 shadow" id="cat_bar">
            <h2 class="ms-3 mt-3 fw-bold fs-4">Categories</h2>
            <ul class="nav flex-column" id="category-list">
            <!-- Catrgory list here -->
            </ul>
        </div>
        <div class="col-md-9 pt-3 pe-4" id="pro_bar">
          <div id="catSubName">
            <span id="catName" class="mt-3 fw-bold fs-4">
                <!-- Category name here -->
            </span>
            <span id="subName" class="mt-3 fw-bold fs-4">
                <!-- Sub Category name here -->
            </span>
            <span id="vrName" class="mt-3 fw-bold fs-4">
                <!-- Variants here -->
            </span>
            <span id="vrDisName" class="mt-3 fw-bold fs-4">
                <!-- Variant name here -->
            </span>
          </div>  
            <div
                class="row row-cols-1 row-cols-md-3 g-4 py-2"
                id="subCategory-list"
            ></div>
            <div class="row" id="varFill" style="display: none">
                <div class="col-md-6" id="varImg">
                    <!-- Variant Image here -->
                </div>
                <div class="col-md-6 grid-center">
                    <div class="d-grid gap-2 col-6" id="proBtn">
                        <!-- User manual, Data Sheet & Enquiry Button here -->
                    </div>
                </div>
                <div class="accordion accordion-flush" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button
                                class="accordion-button"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseOne"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                            >
                                Description
                            </button>
                        </h2>
                        <div
                            id="collapseOne"
                            class="accordion-collapse collapse show"
                            aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body" id="varDesc">
                                <!-- Variant description here -->
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo"
                                aria-expanded="false"
                                aria-controls="collapseTwo"
                            >
                                Technical Specification
                            </button>
                        </h2>
                        <div
                            id="collapseTwo"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                                <img
                                    src="images/techSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button
                                class="accordion-button collapsed"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseThree"
                                aria-expanded="false"
                                aria-controls="collapseThree"
                            >
                                Mechanical Specifications
                            </button>
                        </h2>
                        <div
                            id="collapseThree"
                            class="accordion-collapse collapse"
                            aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample"
                        >
                            <div class="accordion-body">
                                <img
                                    src="images/mechSpec.png"
                                    alt=""
                                    class="img-fluid"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        //Display category name on opening the page for the first time
        //To display subcategories of the first category when the products page is opened for the first time.
        retrieveSubCat(1, "Solar Charge Controllers");

        //Set navigation link active for categories
        // $('ul li a.catLink').on('click', function (){
        //     alert("click");
        //     console.log("click");
            
        // });
    });

    //To display sub categories when user selects a category.
    function retrieveSubCat(catID, cyName) {
        $("#vrDisName").hide();
        $("#subName").hide();
        $("#vrName").hide();
        $("#varFill").hide();
        $("#subCategory-list").show();
        var sendData = { cy_id: catID };
        var catID = catID;
        var catName = cyName;
        $.ajax({
            type: "POST",
            url: "get-category-subCat",
            data: sendData,
            dataType: "json",
            cache: false,
            success: function (response) {
                //console.log(response);
                var allsubCat = "";
                var cyCrumb = "";    
                $.each(response, function (i, subCat) {
                    allsubCat +=
                        "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +
                        subCat.sub_img +
                        "' class='card-img-top pro-img mx-auto' alt='" +
                        subCat.sub_name +
                        "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +
                        subCat.sub_name +
                        "</h6><a href='#' class='btn btn-primary fw-bold' onclick='showProd(\"" +
                        subCat.PK_sub_id +
                        '","' +
                        subCat.sub_name +
                        "\"); return false'>View</a></div></div></div>";
                    cyCrumb = "<a href='#' onclick='retrieveSubCat(\"" +catID+ "\",\"" +cyName + "\"); return false'>"+cyName+"</a>";

                });
                //console.log(allProducts);
                $("#subCategory-list").html(allsubCat);
                $("#catName").html(cyCrumb);
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }

    //To display products when user selects a sub category
    function showProd(subID, subName) {
        $("#vrDisName").hide();
        $("#subName").show();
        $('#vrName').hide();
        $("#varFill").hide();
        $("#subCategory-list").show();
        var sendData = { sub_id: subID };
        var subID = subID;
        var subName = subName;
        $.ajax({
            type: "POST",
            url: "get-subcategory-products",
            data: sendData,
            dataType: "json",
            cache: false,
            success: function (response) {
                //console.log(response);
                var subProducts = "";
                var syCrumb = "";
                $.each(response, function (i, product) {
                    subProducts +=
                        "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +
                        product.pr_img +
                        "' class='card-img-top pro-img mx-auto' alt='" +
                        product.pr_name +
                        "'><div class='card-body'><h6 class='card-title fw-bold fs-5'>" +
                        product.pr_name +
                        "</h6><a href='#' class='btn btn-primary fw-bold mt-2' onclick='showVar(" +
                        product.PK_pr_id +
                        ");return false'>View</a></div></div></div>";
                    syCrumb = "<a href='#' onclick='showProd(\"" + subID +'","'+ subName +"\"); return false'>"+subName+"</a>";
                });
                //console.log(allProducts);
                $("#subCategory-list").html(subProducts);
                $("#subName").html(" / " + syCrumb);
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }
    // To display variants - on clicking view product
    function showVar(prID) {
        $("#vrDisName").hide();
        $('#vrName').show();
        $("#varFill").hide();
        $("#subCategory-list").show();
        var sendData = { prID: prID };
        var prID = prID;
        $.ajax({
            type: "POST",
            url: "get-product-var",
            data: sendData,
            dataType: "json",
            cache: false,
            success: function (response) {
                //console.log(response);
                var variants = "";
                var vrCrumb = "";
                $.each(response, function (i, variant) {
                    variants +=
                        "<div class='col'><div class='card h-100 text-center shadow hover-card'><img src='" +
                        variant.var_img +
                        "' class='card-img-top pro-img mx-auto' alt=''><div class='card-body'><h5 class='card-title fw-bold fs-5'>" +
                        variant.var_details +
                        "</h5><h6 class='card-subtitle mb-2 text-muted'>" +
                        variant.var_mod_no +
                        "</h6><div class='card-footer'><a href='#' class='btn btn-primary fw-bold mt-2' onclick='disVar(\"" +
                        variant.PK_var_id +
                        '","' +
                        variant.var_mod_no +
                        "\");return false' >View</a></div></div></div></div>";
                    vrCrumb = "<a href='#' onclick='showVar("+prID+"); return false'>Variants</a>";
                });
                //console.log(variants);
                $("#subCategory-list").html(variants);
                $("#vrName").html(" / " + vrCrumb);
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }

    function disVar(varID, varMod) {
        $("#vrDisName").show();
        $("#subCategory-list").hide();
        $("#varFill").show();
        var sendData = { varID: varID, varMod: varMod };
        var varMod = varMod;
        //console.log(sendData);
        $.ajax({
            type: "POST",
            url: "get-var-details",
            data: sendData,
            dataType: "json",
            cache: false,
            success: function (response) {
                console.log(response);
                var varImg = "";
                var proBtn = "";
                var varDesc = "";
                var hide_inp = "";
                var EnquiryModalLabel= "";
                var varDisName = "";
                $.each(response, function (i, variant) {
                    varImg +=
                        "<img src='" +
                        variant.var_img +
                        "' alt='' class='img-fluid'/>";
                    proBtn +=
                        "<a class='btn btn-primary' target='_blank' href='" +
                        variant.pr_man +
                        "'>User Manual</a><a class='btn btn-primary' target='_blank' href='" +
                        variant.vr_ds +
                        "'>Data Sheet</a><a class='btn btn-primary' id='enq-btn' data-bs-toggle='modal' data-bs-target='#EnquiryModal' data-bs-dismiss='modal'>Make an enquiry for this product</a>";
                    varDesc += "<p>" + variant.pr_desc + "</p>";
                    hide_inp +=
                        "<input type='text' class='form-control' id='enqVar_mod' value='" +
                        variant.var_mod_no +
                        "-" +
                        variant.var_details +
                        "' disabled>";
                    EnquiryModalLabel += variant.var_details;
                });

                //console.log(varImg);
                $("#varImg").html(varImg);
                $("#proBtn").html(proBtn);
                $("#varDesc").html(varDesc);
                $("#hide_inp").html(hide_inp);
                $("#EnquiryModalLabel").append(EnquiryModalLabel);
                $("#vrDisName").html(" / " + EnquiryModalLabel);
            },
            error: function (e) {
                alert("AJAX Error!");
                console.log("AJAX Error!");
                console.log(e);
            },
        });
    }
</script>
@include ('footerbar') @include ('footer')
