@include ('header')
@include ('navbar')
<!--------------------------------------------------Hero---------------------------------------------------------->
<section>
	<div class="container-fluid products-cover">
		<div class="row">
		 <div class="col-sm-12">
			<h1 class="text-white fw-bold ms-1 mt-3">
				Products
			</h1>
		 </div>
		</div>
	</div>	
</section>
<!------------------------------------------------------------------------------------------------------------>
<section>
	<div class="modal fade" id="varModal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="ModalLabel"></h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		  </div>
		  <div class="modal-body">
			<div class="container-fluid">
				<div class="row">
					<div class="row">
						<div class="col-md-3" id="modalImage">
							
						</div>
						<div class="col-md-9">
							<div class="d-grid gap-2">
							  <button class="btn btn-primary" type="button">User Manual</button>
							  <button class="btn btn-primary" type="button">Data Sheet</button>
							  <button class="btn btn-primary" type="button">Product Brochure</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" id='modalDesc'>
							
						</div>
					</div>
				</div>
				<div class="row">
					<table class='table'>
						<thead>
							<tr>
								<th scope='col'>Model No.</th>
								<th scope='col'>Details</th>
								<th scope='col'>Enquire</th>
							</tr>
						</thead>
						<tbody id="bodyTable">
						
						</tbody>
					</table>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			
		  </div>
		</div>
	  </div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<h2 class="fs-5 mt-3">Categories</h2>
				<ul class="nav flex-column" id="category-list">

				</ul>
			</div>
			<div class="col-md-9" >
				<div class="row  row-cols-1 row-cols-md-3 g-4" id="subCategory-list">
			
				</div>
			</div>
		</div>
	</div>
</section>
<script>
$(document).ready(function(){
	$.ajaxSetup({
			  headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});
//Displaying all the categories in the vertical navbar
	$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
            dataType: "json",                    
            cache: false,                       
           	success: function(response) 
				{
					//console.log(response);
					var catList = '';
					var passCatID='';
					$.each(response, function(i, category)
						{ 	
							catList += "<li class='nav-item'><a class='nav-link ps-0' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(" +category.PK_cy_id+ "); return false' > "+category.cy_name+ "</a></li>";
						});
					//console.log(catList);	
					$('#category-list').append(catList);
				},
				error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
		});
	
//To display subcategories of the first category when the products page is opened for the first time.
	var sendData = {'cy_id': 1};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+"); return false'><div class='card h-100 text-center'><img src='" +subCat.sub_img+
							"' class='card-img-top' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
	
	
});

//To display sub categories when user selects a category.
function retrieveSubCat(catID){
	//var test = catID;
	//console.log(test);
	var sendData = {'cy_id': catID};
	$.ajax({
			type: "POST",
			url: "get-category-subCat",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var allsubCat = '';
				$.each(response, function(i, subCat)
					{ 	
						allsubCat += "<div class='col'><a href='#' onclick='showProd("+subCat.PK_sub_id+"); return false'><div class='card h-100 text-center'><img src='" +subCat.sub_img+
							"' class='card-img-top' alt='" +subCat.sub_name+ "'><div class='card-body'><h6 class='card-title'>" +subCat.sub_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(allsubCat);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display products when user selects a sub category
function showProd(subID){
	
	var sendData = {'sub_id': subID};
	$.ajax({
			type: "POST",
			url: "get-subcategory-products",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var subProducts = '';
				$.each(response, function(i, product)
					{ 	
						subProducts += "<div class='col'><a href='#' data-bs-toggle='modal' data-bs-target='#varModal' onclick='varModal("+product.PK_pr_id+")'><div class='card h-100 text-center'><img src='" +product.pr_img+
						"' class='card-img-top' alt='" +product.pr_name+ "'><div class='card-body'><h6 class='card-title'>" +product.pr_name+ "</h6></div></div></a></div>"
					});
				//console.log(allProducts);
				$('#subCategory-list').html(subProducts);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});		
};

//To display product variant details in the modal when user clicks on a product.

function varModal(prID)
{
	var sendData = {'sub_id': prID};
	$.ajax({
			type: "POST",
			url: "get-product-var",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var prodVar = '';
				$.each(response, function(i, variant)
					{ 	
						prodVar += "<tr><td>"+variant.var_mod_no+"</td><td>"+variant.var_details+"</td><td><a href='#'>Enquire</a></td></tr>"
						
						
					});
				//console.log(allProducts);
				$('#bodyTable').html(prodVar);
				//$('#ModalLabel').html();
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},
							
			});
			
	$.ajax({
			type: "POST",
			url: "get-prod-modal",
			data: sendData,
           	dataType: "json",                    
      		cache: false,                       
            success: function(response) 
			{	
				//console.log(response);
				var imagesModal = '';
				var descModal = '';
				$.each(response, function(i, product)
					{ 	
						imagesModal += "<img src='"+product.pr_img+"' class='img-fluid'>";
						
						descModal += "<p>"+product.pr_desc+"</p>";
					});
				//console.log(allProducts);
				$('#modalImage').html(imagesModal);
				$('#modalDesc').html(descModal);
			},
			error: function(e)
					{
						alert('AJAX Error!');
						console.log('AJAX Error!');
						console.log(e);
					},		
			});			
			
			
}
</script>
@include ('footer')
