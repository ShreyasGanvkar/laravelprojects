<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\VariantController;
use App\Http\Controllers\EnquiryController;
use App\Http\Controllers\SendMailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Pages
Route::get('/',[PageController::class, 'index']);
Route::get('/index',[PageController::class, 'index']);
Route::get('/aboutus',[PageController::class, 'aboutus']);
Route::get('/products',[PageController::class, 'products']);
// Route::get('/contactus',[PageController::class, 'contactus']);

//AJAX
Route::post('get-categories',[CategoryController::class, 'getAllCat']);	            //get all categories
Route::post('get-subCategories',[SubcategoryController::class, 'getAllSubCat']);    //get all subCategories
Route::post('get-subcategory-products',[ProductController::class, 'getSpeProd']);   //get specific Products
Route::post('get-category-subCat',[SubcategoryController::class, 'catSubCat']);     //get specific subCategories
Route::post('get-product-var',[VariantController::class, 'getSpeProdVar']);         //get specific Variant

Route::post('get-prod-modal',[ProductController::class, 'catProducts']);            // Product modal

Route::post('get-var-mod',[VariantController::class, 'getVarNo']);                  //Product modal

Route::post('submit-form-db',[EnquiryController::class, 'insert']);                 // Enquiry form submit to DB

Route::post('submit-form-email',[SendMailController::class, 'send']);               // Enquiry form send mail

Route::post('get-cat-name',[CategoryController::class, 'getSpeCat']);

Route::post('get-var-details',[VariantController::class, 'getVarDet']);                  //Get variant details

//testing 
//Route::post('get-spe-subCategories',[SubcategoryController::class, 'catSubCat']); //get Category subCategories
Route::view('/products1','products1');
Route::view('/products2','products2');
Route::view('/products3','products3');
Route::view('/aboutus2','aboutus2');
Route::view('/products4','products4');