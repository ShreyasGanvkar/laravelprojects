<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiry', function (Blueprint $table) {
            $table->increments('PK_enq_id');
			$table->text('name');
			$table->string('num');
			$table->string('altnum');
			$table->string('email');
			$table->text('enq');
			$table->unsignedInteger('FK_var_id');
			$table->foreign('FK_var_id')->references('Pk_var_id')->on('variants');
            $table->string('var_mod');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiry');
    }
}
