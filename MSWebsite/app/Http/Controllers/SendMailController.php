<?php

namespace App\Http\Controllers;

use App\Mail\EnquiryMail;
use Illuminate\Http\Request;
use Mail;
use Validator;


class SendMailController extends Controller
{
    //
    public function send(Request $request)
    {   
        //\Log::info($request);
        $details= $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
             'num' => 'required',
             'altnum' => 'nullable',
             'email' => 'required|email',
             'enq' => 'required',
             'vr_id' => 'required',
             'var_mod' =>'required',
         ]);
         
         if ($validator->passes()) {
            
            Mail::to('testingshreyas@gmail.com')->send(new EnquiryMail($details));
 
             return json_encode(array(['success'=>'Mail Sent']));
         }
 
         else{
             return json_encode(array(['error'=>$validator->errors()->all()]));	
         }

        
    }
}
