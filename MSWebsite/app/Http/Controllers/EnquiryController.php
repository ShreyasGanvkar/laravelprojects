<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Log;


class EnquiryController extends Controller
{
	public function insert(Request $request){
		
		//\Log::info($request);

		$validator = Validator::make($request->all(), [
           'name' => 'required',
			'num' => 'required',
			'altnum' => 'nullable',
			'email' => 'required|email',
			'enq' => 'required',
			'vr_id' => 'required',
			'var_mod' =>'required',
        ]);
		
		if ($validator->passes()) {
           
		\DB::table('enquiry')->insert([
			'name' => $request->name,
			'num' => $request->num,
			'altnum' => $request->altNum,
			'email' => $request->email,
			'enq' => $request->enq,
			'FK_var_id' => $request->vr_id,
			'var_mod' =>$request->var_mod,
			'created_at' =>  \Carbon\Carbon::now(), 
            'updated_at' => \Carbon\Carbon::now()
			]);

			return json_encode(['success'=>'Added new records.']);
		}

		else{
			return json_encode(['error'=>$validator->errors()]);
		}
	
	}	
}

