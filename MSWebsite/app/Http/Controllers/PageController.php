<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;

class PageController extends Controller
{
    public function index()
	{
		return view('index');
	}
	
	 public function aboutus()
	{
		return view('aboutus');
	}
	
	public function products()
	{
		return view('products');
		// $categories = Category::all();
		// //\Log::info($categories);
		// return view('products',compact('categories'));
	}

	public function contactus()
	{
		return view('contactus');
	}
}
