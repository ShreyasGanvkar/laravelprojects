<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\SubCategory;

class SubcategoryController extends Controller
{
	 public function getAllSubCat(){
		//DB::enableQueryLog(); // Enable query log
	
		$qsubCatSelAll=DB::table('subcategories')
            ->select('PK_sub_id', 'sub_name','sub_img','FK_cy_id')
            ->get();
			
		//\Log::debug(DB::getQueryLog());
		//\Log::info($qsubCatSelAll);
		return response()->json($qsubCatSelAll);
		
	}
	
	public function catSubCat(Request $catID)
	{
		$input = $catID->all();
		$subCat=SubCategory::select("*")
			->where("FK_cy_id","=",$input)
			->get();
		//\Log::info($subCat);
		return response()->json($subCat);
	}
}
