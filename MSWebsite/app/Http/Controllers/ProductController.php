<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    //
	public function getSpeProd(Request $subID)
	{	
		$input = $subID->all();
		$products=Product::select("*")
			->where("FK_sub_id","=",$input)
			->get();
		//\Log::info($products);
		return response()->json($products);
	}
	
	public function catProducts(Request $catID)
	{
		$input = $catID->all();
		$products=Product::select("*")
			->where("PK_pr_id","=",$input)
			->get();
		//\Log::info($products);
		return response()->json($products);
	}	
}
