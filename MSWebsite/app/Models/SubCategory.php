<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Category;
use App\Models\Product;

class SubCategory extends Model
{
    //use HasFactory;
	protected $table = 'subcategories';
	
	public function cat()
	{
		return $this->belongsTo(Category::class);
	}
	
	public function prod()
	{
		return $this->hasMany(Product::class);
	}
}
