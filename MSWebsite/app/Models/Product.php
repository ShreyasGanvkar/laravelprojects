<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\SubCategory;
use App\Models\Variant;

class Product extends Model
{
    //use HasFactory;
	
	public function catsub()
	{
		$this->belongsTo(SubCategory::class);
	}
	
	public function variant()
	{
		$this->hasMany(Variant::class);
	}
}
