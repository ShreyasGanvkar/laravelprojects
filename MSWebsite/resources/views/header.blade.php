<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content="" name="description">
  	<meta content="" name="keywords">
	<!--Custom Font-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/index.css') }}" rel="stylesheet">
	
	<!-- Slick Carousel -->
	<link href="{{ asset('css/slick.css') }}" rel="stylesheet" />
	
	<script src="{{ asset('js/jquery-3.6.0.js') }}"></script>
	<script src="{{ asset('js/slick.js') }}"></script>

	<!-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js" > </script> -->
	<script src="{{ asset('js/validate.js') }}" > </script>

	<!--Icons-->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<link href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  	<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">

    <title>Maruthi Solar Systems</title>
  </head>
  <body>