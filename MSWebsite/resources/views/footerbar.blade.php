@include ('header')
  <!-- ======= Footer ======= -->
  <footer id="footer" class="border-1 border-top">

    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>Maruthi Solar Systems</h3>
            <p>
            # 41,42,43, Sy. No. 10/1<br>
            Abbigere Main Road<br>
						Near Bus Stop,<br>
						Kereguddadahalli<br>
						Bangalore - 560090.<br><br>
              <strong>Phone:</strong> +91-80-23253889<br>
              <strong>Email:</strong> info@maruthisolar.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Explore</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Products</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Clients</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contact us</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Production</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contract Manufacturing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">OEM</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Job Contraction</a></li>
            </ul>
          </div>

          <!-- <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Let us get in touch with you</h4>
            <p>Enter your number down below an we will call you.</p>
            <form action="" method="post">
              <input type="tel" name="phone"><input type="submit" value="Submit">
            </form>
          </div> -->

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Products</h4>
            <ul id='footerProdList'>
              <!-- Products list here -->
            </ul>
          </div>

        </div>
      </div>
    </div>
    <div class="container d-md-flex py-4">

  <div class="me-md-auto text-center text-md-start">
    <div class="copyright">
      &copy; Copyright <strong><span>Maruthi Solar Systems</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- Designed by <a href='#'></a> -->
    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
  </div>
  </footer><!-- End Footer -->
	<script>
$(document).ready(function(){
	$.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
    //Displaying all the categories in the footer			
	//Displaying all the categories in the vertical navbar - Products page
	$.ajax({
			type:"POST",
			url: "get-categories",
			data: {},
			dataType: "json",                    
			cache: false,                       
			success: function(response) 
				{
					//console.log(response);
						
					var footerProdList ='';
					var catList = '';
					$.each(response, function(i, category)
						{ 	   	
							footerProdList += "<li><i class='bx bx-chevron-right'></i> <a href='products'>"+ category.cy_name +"</a></li>";
							catList += "<li class='nav-item catLi' id='hlt'><a class='nav-link catLink' href='#' id='category-"+category.PK_cy_id+"' onclick='retrieveSubCat(\"" +category.PK_cy_id+ "\",\"" +category.cy_name + "\"); return false' > "+category.cy_name+ "</a></li>";
						});
						//console.log(sliSlide);	
					$('#footerProdList').append(footerProdList);
					$('#category-list').append(catList);
					},
					error: function(e)
						{
							alert('AJAX Error!');
							console.log('AJAX Error!');
							console.log(e);
						},
								
			})
			.done(function(){ 
				//Highlight the selected element on products page categories navbar
				$("#hlt").addClass("selected");  
				$('#category-1').addClass('text-white');
				$('a.catLink').click(function(e) {
					$('.catLi.selected').removeClass('selected');
					$('.catLink.text-white').removeClass('text-white');
					var $parent = $(this).parent();
					var $child = $(this).parent().children();
					$parent.addClass('selected');
					$child.addClass('text-white');
					e.preventDefault();
	 			});	
			});
});
</script>