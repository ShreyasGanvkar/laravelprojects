@include ('header')
@include ('navbar')

<!-------------------------------------------------------Hero----------------------------------------------------->

 <section id="hero" class="d-flex align-items-center">
  <div class="container">
	<h1 class="ms-3 span-yellow fw-bold">Ideate. Innovate. Integrate.</h1>
    <h2 class="ms-3 span-yellow">Redefining solar energy in India since 1998.</h2>
	<a href="#" class="btn btn-get-started" role="button">View our products</a>
  </div>
 </section>	
     <!------------------------------------------------About us----------------------------------------------->
     <section id="about-us" class="about-us border-2 border-bottom">
      <div class="container">

        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>About us</h3>
              <p>
              Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products..
              </p>
              <div class="text-center">
                <a href="#" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
			  <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Our Services</h4>
                    <p >Design<br/>Production<br/>Contract Manufacturing<br/>OEM<br/>Job Contraction</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-receipt"></i>
                    <h4>Our Vision</h4>
                    <p>To provide the most compelling value in the solar energy industry.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Our Mission</h4>
                    <p>To accelerate the adoption of solar energy systems by providing unparalleled value.</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End of About us -->
       <!-- ======= Why us Section ======= -->
       <section id="why-us" class="why-us border-2 border-bottom">
      <div class="container">

        <div class="section-title">
          <h2>Why us?</h2>
          <p>An ISO 9001:2015 certified company.</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch ">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-cogs"></i></div>
              <h4><a href="">Engineering</a></h4>
              <p>Customer requirements and site conditions are considered when designing the system.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 ">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-users"></i></div>
              <h4><a href="">Research & Development</a></h4>
              <p>In-house R&D provides faster solutions to meet customer requirements.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-industry"></i></div>
              <h4><a href="">Manufacturing</a></h4>
              <p>ISO 9001:2015 certified production facility.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-microchip"></i></div>
              <h4><a href="">I & C</a></h4>	 
              <p>Highly experienced and dedicated team with engineers for faster and successful execution.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-graduation-cap"></i></div>
              <h4><a href="">Expertise</a></h4>
              <p>Four decades of experience in design and manufacturing.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box shadow w-100">
              <div class="icon"><i class="fa fa-phone"></i></div>
              <h4><a href="">After Sales Services</a></h4>
              <p>Guaranteed after sales support in various locations.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why us Section -->
<!--------------------------------------------------Products Carousel---------------------------------------->	
	<section class="border-2 border-bottom">
	  <div class="container">
		<div class="section-title">
          <h2>Our Products</h2>
        </div>
		<div id="slick" class="">

		  <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		   <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-01.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-02.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="">
			  <img src="{{ asset('images/charge-c-03.jpg') }}" class="carousel-card-img" />
			</div>
		  </div>

		</div>
        <div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn pro-btn">VIEW OUR PRODUCTS</a>
				</div>
			</div>
	  </div>
</section>
<!-------------------------------------------Some of our projects------------------------------------------------>
	<section class="border-2 border-bottom">
		<div class="container">
		 <div class="section-title">
          <h2>Our Projects</h2>
         </div>
			<div class="row mt-4 mb-3">
				<div class="col-md-4 flexing">
					<div class="card shadow" >
						<img src="{{ asset('images/power-plant-1.jpg') }}" class="card-img-top " alt="..." >
						<div class="card-body text-center">
							<p class="card-text fw-bold">3.5KW at RR Temple, Bangalore through BHEL, Bangalore</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 flexing">
					<div class="card shadow ">
						<img src="{{ asset('images/power-plant-4.jpg') }}" class="card-img-top ">
						<div class="card-body text-center">
							<p class="card-text fw-bold">42KW at ESD, BHEL, Bangalore thorough BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing">
					<div class="card shadow">
						<img src="{{ asset('images/power-plant-6.jpg') }}" class="card-img-top ">
						<div class="card-body text-center">
							<p class="card-text fw-bold">40KW at Engineering College, Kanchipuram through BHEL</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-12 text-center pt-md-6">
				<a href="#" class="btn pro-btn">VIEW OUR PROJECTS</a>
				</div>
			</div>	
		</div>
	</section>
<!---------------------------------------------------Customer Testimonials------------------------------------>	
	<section>
		<div class="container">
			<div class="section-title">
          	 <h2>Customer Testimonials</h2>
         	</div>
			<div class="row">
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bhel.gif') }}" class="card-title">
							<p class="card-text">“In a single contact, we received detailed and timely assistance. For improved quality,
							I recommend this company and its professional engineers.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/reliance-solar-group.gif') }}" class="card-title">
							<p class="card-text">“I always say that the quality and service at this organisation are fantastic, and this is just another example of the positive work you all do there.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="{{ asset('images/bharat-electronics.gif') }}" class="card-title">
							<p class="card-text">“This team provides exceptional service and assistance both before and after the purchase.”</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="#" class="btn pro-btn">View Our Projects</a>
				</div>
			</div>
		</div>
	</section>


@include ('footerbar')
@include ('footer')

<script>
var breakpoint = {
  // Small screen / phone
  sm: 576,
  // Medium screen / tablet
  md: 768,
  // Large screen / desktop
  lg: 992,
  // Extra large screen / wide desktop
  xl: 1200
};

// page slider
$('#slick').slick({
  
  autoplay: true,
  autoplaySpeed: 0,
  draggable: true,
  infinite: true,
  dots: false,
  arrows: false,
  speed: 10000,
  mobileFirst: true,
  cssEase: 'linear',
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: breakpoint.sm,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: breakpoint.md,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: breakpoint.lg,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: breakpoint.xl,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5
      }
    }
  ]
});

</script>	

