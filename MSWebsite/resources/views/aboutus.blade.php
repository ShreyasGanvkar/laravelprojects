@include ('header')
@include ('navbar')

<!-------------------------------------------------------Hero--------------------------------------------------->
<section class="breadcrumbs mt-0">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2 class="text-white">About us</h2>
          <ol>
            <li><a href="/aboutus" class="text-white">Home</a></li>
            <li >About us</li>
          </ol>
        </div>

      </div>
</section><!-- End Breadcrumbs Section -->
<!--------------------------------------------------Who we are--------------------------------------------------->
<section class="pt-0">
	<div class="container">
		<div class="row h-100">
			<div class="col-md-4 col-sm-12 d-none d-sm-flex" id="who_we-head">
				<h2 class="fw-bold text-center"><span class="display-5 fw-bold">Who We Are</span></br></br>An</br><a href="#iso-cert" class="text-deco" >ISO 9001-2015</a></br> certified company</h2>
			</div>
			<!--For mobile-->
			<div class="col-md-4 col-sm-12 d-block d-sm-none">
				<h2 class="fw-bold text-center"><span class="display-1">Who We Are</span></br></br>An</br> 
					<a href="#iso-cert" class="text-deco">ISO 9001-2015</a></br> certified,</br>
					<a href="#nsic-cert" class="text-deco"> NSIC/MSME </a></br>registered </br>&
					<a href="#certificates" class="text-deco"></br>MNRE </a></br>licensed company
				</h2>
			</div>
			<!-- -->
			<div class="col-md-1 flexing pe-0">
				<div class="line1"></div>
			</div>
			<div class="col-md-7 col-sm-12 flexing ps-0">
				<p class="pt-4 who-we_p who-we_mobile-m fw-bold">Maruthi Solar Systems was started in the year 1998. It is based in Bangalore, India and is solely dedicated to designing , manufacturing and supplying in the line of electronic products related to power electronics, renewable energy and other related products.</br></br>
				We are proud to claim that we are one of the major contract manufacturers of electronic products for many OEMs like Bharat Electronics, BHEL, Tata Power Solar, Reliance Solar and many other private system integrators in India.</br></br>
				Our company is licensed by MNRE to support projects sponsored by Government of India. We have a R&D based manufacturing set up in Bangalore, India under the company banner Maruthi Solar Systems with manufacturing area of 6000 sq ft.
				</p>
			</div>
		</div>
	</div>
</section>
<!------------------------------------------------------ISO--------------------------------------------------->
<section>
	<div class="container-fluid">
		<div class="row h-100 pt-md-5 mb-4">
			<div class="col-md-4 d-none d-sm-block" id="iso_head">
				<h2 class="fw-bold text-center pt-5"><span class="display-6 fw-bold">We are</span></br><a href="#nsic-cert" class="text-deco">NSIC/MSME</a></br> registered</br>& </br><a href="#certificates" class="text-deco">MNRE</a></br>licensed</h2>
			</div>
			<!--For mobile-->
			<!--div class="col-md-4 col-sm-12 d-block d-sm-none">
				<h2 class="fw-bold fs-2"><a href="#nsic-cert" class="text-deco">NSIC/MSME </a>registered</br> & </br><a href="#certificates" class="text-deco">MNRE</a> licensed</h2>
			</div-->
			<!-- -->
			<div class="col-md-1 flexing pe-0">
				<div class="line2 d-none d-sm-block" ></div>
			</div>
			<div class="col-md-7 col-sm-12 flexing ps-0">
				<p class=" who-we_mobile-m fw-bolder">Our main strength lies in understanding the customer requirements, meticulous planning, dedication and hard work of our team. We also specialize in design, manufacturing, supply and installation of solar lighting systems, off-Grid and on-Grid solar systems.</br></br>
 
               With 22 years of hard efforts, we are becoming more and more internationalized through our passionate employees and strong <s>cooperative</s> R&D capabilities. We can react quickly to our customer’s needs and provide comprehensive, customized & complete products.
				</p>
			</div>
		</div>
	</div>
</section>
<!------------------------------------------------Our Strengths--------------------------------------------------->
<section class="cover-strength">
	<div class="container-fluid py-5">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h1 class="text-center fw-bold pb-5 text-white">Our Strengths</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12 flexing anc-str">
				<div class="card text-center border-0 card-str" >
					<img src="{{ asset('images/eng2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">Engineering</span>
					<p class="card-text text-white">Design the system according to the requirement of the customer and the site conditions.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 flexing">
				<div class="card text-center border-0 card-str">
					<img src="{{ asset('images/research2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">R&D</span>
					<p class="card-text text-white">Our in-house R&D caters to</br> customer requirements with speedy solutions.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 flexing">
				<div class="card text-center border-0 card-str" >
					<img src="{{ asset('images/mfg1.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">Manufacturing</span>
					<p class="card-text text-white">Our manufacturing facility is</br> ISO 9001:2015</br> certified.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-card_ic" >
				<div class="card text-center border-0 card-str" id="card-ic">
					<img src="{{ asset('images/ic1.png') }}" class=" mx-auto">
					<span class="card-title mt-2 fw-bold text-white">I & C</span>
					<p class="card-text text-white">Highly experienced and dedicated team with engineers for faster and successful execution.</p>
				</div>
			</div>
			<!--div class="col-md-6 col-card_ass override" -->
			<div class="col-md-6 col-card_ass" >
				<div class="card text-center border-0 card-str" id="card-ass">
					<img src="{{ asset('images/S&S2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">After Sales Services</span>
					<p class="card-text text-white">Guaranteed after sales support & services for businesses situated in various locations.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!--------------------------------------------------Certificates-------------------------------------------------->
<section id="certificates">
	<div class="container-fluid mb-5">
		<!--ISO Modal-->
		<div class="modal fade" id="isoModal" tabindex="-1" aria-labelledby="isoModalLabel" aria-hidden="true">
		  <div class="modal-dialog ">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="isoModalLabel">ISO 9001:2015</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="{{ asset('images/iso.jpg') }}" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--MSME Modal-->
		<div class="modal fade" id="msmeModal" tabindex="-1" aria-labelledby="msmeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="msmeModalLabel">MSME</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="{{ asset('images/msme.jpg') }}" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--NSIC Modal-->
		<div class="modal fade" id="nsicModal" tabindex="-1" aria-labelledby="nsicModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="nsicModalLabel">NSIC</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<div class="container-fluid">
					<div class="row">
					  <div class="col-md-6">
						<img src="{{ asset('images/nsic1.jpg') }}" class="img-fluid mb-3">
					  </div>
					  <div class="col-md-6">
						<img src="{{ asset('images/nsic2.jpg') }}" class="img-fluid">
					  </div>
					</div>
				</div>	
			  </div>
			</div>
		  </div>
		</div>
		<div class="row py-4">
			<div class="col-md-12 col-sm-12">
				<h1 class="text-center fw-bold">Certificates</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12" id="iso-cert">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#isoModal" class="mx-auto">
						<img src="{{ asset('images/iso.jpg') }}" class="cert-img" >
					</a>	
					<div class="card-title text-center fw-bold mt-3">ISO 9001:2015</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#msmeModal" class="mt-md-5" >
						<img src="{{ asset('images/msme.jpg') }}" class="img-fluid" >
					</a>
					<div class="card-title text-center fw-bold mt-3">MSME</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12" id="nsic-cert">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#nsicModal" class="mx-auto">
						<img src="{{ asset('images/nsic1.jpg') }}" class="cert-img" >
					</a>
					<div class="card-title text-center fw-bold mt-3">NSIC</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include ('footerbar')
@include ('footer')