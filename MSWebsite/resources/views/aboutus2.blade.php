@include ('header')
@include ('navbar')

<!-------------------------------------------------------Hero--------------------------------------------------->
<section class="breadcrumbs mt-0">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2 class="text-white">About us</h2>
          <ol>
            <li><a href="/aboutus" class="text-white">Home</a></li>
            <li >About us</li>
          </ol>
        </div>

      </div>
</section><!-- End Breadcrumbs Section -->
<!--------------------------------------------------Who we are--------------------------------------------------->
<section id="who-we-are" class="who-we-are border-2 border-bottom">
      <div class="container" data-aos="fade-up">

        <div class="row content">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" >
             <div class="line ps-4">
            <h2>Who We are</h2>
            <h3 class="text-muted">Maruthi Solar Systems, an ISO 9001:2015 accredited firm, was established in 1998 in Bengaluru, India.</h3>
            </div> 
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 " data-aos="fade-left">
            <p class="fs-5">
            The company specializes in the design, production, supply and installation of electronic devices, related to power electronics, renewable energies (Off Grid and On Grid Solar systems), energy-efficient LED products for AC & DC lighting systems, and other related products.
            </p>
            <p class="fs-5">
            We are proud to claim that we are one of the major contract manufacturers of electronic products for many OEMs like Bharat Electronics, BHEL, Tata Power Solar, Reliance Solar and many other private system integrators in India. 
            </p>
            <p class="fs-5">
            Our biggest asset is our team's ability to anticipate the needs of our customers, prepare meticulously, and work tirelessly. Our group company is licensed by MNRE to support project sponsored by Government of India. 
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->
    <section class="who-we-are border-2 border-bottom">
      <div class="container" data-aos="fade-up">

        <div class="row content">
          <div class="col-lg-6 d-flex align-items-center"  data-aos="fade-right" >
           <div class="line ps-4">
          <h2>Our Leader</h2>
          <h3 class="text-muted">Mr. B.H. NAYAK is the company's founder and CEO.</h3>
          </div>   
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
            <p class="fs-5">
            He is a successful entrepreneur with four decades of experience in the design, manufacturing and installation of renewable energy, power electronics, telecom and lighting.
            </p>
            <p class="fs-5">
            He specialises in business development and is ultimately responsible for the company's business decisions, which include those in logistics, communications, financing, and human resources. He is leading the business to greater heights and becoming the best in the industry with his experience, never say die approach, high energy levels, and pleasing personality.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section --> 
    <section class="who-we-are border-2 border-bottom">
      <div class="container" data-aos="fade-up">

        <div class="row content">
          <div class="col-lg-6 d-flex align-items-center"  data-aos="fade-right">
            <div class="line ps-4">
            <h2>Our Infrastructure</h2>
            <h3 class="text-muted">Under the company banner MARUTHI SOLAR SYSTEMS, we have our own R&D focused manufacturing set up in Bangalore, India, with a manufacturing area of 6000 sq ft.</h3>
            </div>  
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
            <p class="fs-5">
             Our 23 years of hard work, passionate employees and solid cooperative R&D skills have made us increasingly internationalised. We are able to respond rapidly to the needs of our customers and deliver comprehensive, customised, and complete products. We have our own warehouse where we stock and deliver goods to the customer's location with the utmost care and security.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section --> 
	<section class="who-we-are border-2 border-bottom">
      <div class="container" data-aos="fade-up">

        <div class="row content">
          <div class="col-lg-6 d-flex align-items-center"  data-aos="fade-right">
            <div class="line ps-4">
            <h2>Commitment to Quality</h2>
            <h3 class="text-muted">We aim for the best possible quality that suits the needs of our customers. We also provide data-improvement services for these goods.</h3>
            </div>  
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
            <p class="fs-5">
			At MARUTHI SOLAR SYSTEMS, all employees are aware of, and follow, our quality policy in their daily activities. We work hard to ensure that our goods and services match, and exceed, our customers’ needs and expectations.
            </p>
			<p class="fs-5">
			We are committed to continuous improvements and – our Quality Management System provides a framework for measuring and improving our products and performance.
			</p>
			<p class="fs-5">
			The following systems and procedures are in place to support our aim of total customer satisfaction and continuous improvement:
			</p>
			<ul class="fs-5">
				<li class="pt-0"><i class="bx bx-chevron-right"></i>ISO 9001:2015 </li>
				<li class="pt-0"><i class="bx bx-chevron-right"></i>Training and development for our employees</li>
				<li class="pt-0"><i class="bx bx-chevron-right"></i>Extensive Product Testing & releasing processes</li>
			</ul>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<!------------------------------------------------Our Strengths--------------------------------------------------->
<section class="cover-strength">
	<div class="container-fluid">
        <div class="section-title">
          <h2 class="text-white fs-1">Our Strengths</h2>
        </div>
		<div class="row">
			<div class="col-md-4 col-sm-12 flexing anc-str">
				<div class="card text-center border-0 card-str" >
					<img src="{{ asset('images/eng2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">Engineering</span>
					<p class="card-text text-white">Design the system according to the requirement of the customer and the site conditions.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 flexing">
				<div class="card text-center border-0 card-str">
					<img src="{{ asset('images/research2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">R&D</span>
					<p class="card-text text-white">Our in-house R&D caters to</br> customer requirements with speedy solutions.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 flexing">
				<div class="card text-center border-0 card-str" >
					<img src="{{ asset('images/mfg1.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">Manufacturing</span>
					<p class="card-text text-white">Our manufacturing facility is</br> ISO 9001:2015</br> certified.</p>
				</div>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-6 col-card_ic" >
				<div class="card text-center border-0 card-str" id="card-ic">
					<img src="{{ asset('images/ic1.png') }}" class=" mx-auto">
					<span class="card-title mt-2 fw-bold text-white">I & C</span>
					<p class="card-text text-white">Highly experienced and dedicated team with engineers for faster and successful execution.</p>
				</div>
			</div>
			
			<div class="col-md-6 col-card_ass" >
				<div class="card text-center border-0 card-str" id="card-ass">
					<img src="{{ asset('images/S&S2.png') }}" class="mx-auto">
					<span class="card-title mt-2 fw-bold text-white">After Sales Services</span>
					<p class="card-text text-white">Guaranteed after sales support & services for businesses situated in various locations.</p>
				</div>
			</div>
		</div> -->
	</div>
</section>
<!--------------------------------------------------Certificates-------------------------------------------------->
<section id="certificates">
	<div class="container-fluid mb-5">
		<!--ISO Modal-->
		<div class="modal fade" id="isoModal" tabindex="-1" aria-labelledby="isoModalLabel" aria-hidden="true">
		  <div class="modal-dialog ">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="isoModalLabel">ISO 9001:2015</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="{{ asset('images/iso.jpg') }}" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--MSME Modal-->
		<div class="modal fade" id="msmeModal" tabindex="-1" aria-labelledby="msmeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="msmeModalLabel">MSME</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<img src="{{ asset('images/msme.jpg') }}" class="img-fluid">
			  </div>
			</div>
		  </div>
		</div>
		<!--NSIC Modal-->
		<div class="modal fade" id="nsicModal" tabindex="-1" aria-labelledby="nsicModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="nsicModalLabel">NSIC</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>
			  <div class="modal-body">
				<div class="container-fluid">
					<div class="row">
					  <div class="col-md-6">
						<img src="{{ asset('images/nsic1.jpg') }}" class="img-fluid mb-3">
					  </div>
					  <div class="col-md-6">
						<img src="{{ asset('images/nsic2.jpg') }}" class="img-fluid">
					  </div>
					</div>
				</div>	
			  </div>
			</div>
		  </div>
		</div>
		<div class="row py-4">
			<div class="col-md-12 col-sm-12">
				<h1 class="text-center fw-bold">Certificates</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12" id="iso-cert">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#isoModal" class="mx-auto">
						<img src="{{ asset('images/iso.jpg') }}" class="cert-img" >
					</a>	
					<div class="card-title text-center fw-bold mt-3">ISO 9001:2015</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#msmeModal" class="mt-md-5" >
						<img src="{{ asset('images/msme.jpg') }}" class="img-fluid" >
					</a>
					<div class="card-title text-center fw-bold mt-3">MSME</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12" id="nsic-cert">
				<div class="card border-0">
					<a href="#" data-bs-toggle="modal" data-bs-target="#nsicModal" class="mx-auto">
						<img src="{{ asset('images/nsic1.jpg') }}" class="cert-img" >
					</a>
					<div class="card-title text-center fw-bold mt-3">NSIC</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include ('footerbar')
@include ('footer')