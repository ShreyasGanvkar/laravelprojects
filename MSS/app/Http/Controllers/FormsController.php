<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Models\Form;

class FormsController extends Controller
{
    //
	public function insert(Request $request){
		
		//\Log::info($request);
		
		//DB::enableQueryLog();
		
        //Form::create($request->all());
		
		//\Log::debug(DB::getQueryLog());
		
		
		\DB::table('forms')->insert([
			'name' => $request->name,
			'desig' => $request->dsg,
			'org' => $request->org,
			'email' => $request->email,
			'city' => $request->city,
			'conNo' => $request->conno,
			'altConno' => $request->altConno,
			'requirements' => $request->req,
			'created_at' =>  \Carbon\Carbon::now(), # new \Datetime()
            'updated_at' => \Carbon\Carbon::now(),  # new \Datetime()
			]);
		
		return json_encode(array(
            "statusCode"=>true
        ));		

	}
}