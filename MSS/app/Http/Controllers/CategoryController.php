<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller
{
    //
	public function getCategories()
	{
		$categories=Category::all();
		//\Log::info($categories);
		return response()->json($categories);
	}
}
