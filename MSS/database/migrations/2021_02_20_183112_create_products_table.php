<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('pr_id');
			$table->string('pr_name',50);
			$table->string('pr_desc',1000);
			$table->string('pr_img',100);
			$table->string('pr_feat',1000);
			$table->string('pr_pdf',100);
			$table->integer('pr_cy_id')->unsigned();
			$table->foreign('pr_cy_id')->references('cy_id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
