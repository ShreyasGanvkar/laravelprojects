-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: mswebsite
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `PK_cy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cy_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cy_img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`PK_cy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Solar Charge Controllers',NULL,NULL,'images/charge-c-02.jpg'),(2,'Solar Inverters',NULL,NULL,'images/solar_inveter.jpg'),(3,'Solar Street Lights',NULL,NULL,'images/charge-c-02.jpg');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquiry`
--

DROP TABLE IF EXISTS `enquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enquiry` (
  `PK_enq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `altnum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enq` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_var_id` int(10) unsigned NOT NULL,
  `var_mod` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PK_enq_id`),
  KEY `enquiry_fk_var_id_foreign` (`FK_var_id`),
  CONSTRAINT `enquiry_fk_var_id_foreign` FOREIGN KEY (`FK_var_id`) REFERENCES `variants` (`PK_var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquiry`
--

LOCK TABLES `enquiry` WRITE;
/*!40000 ALTER TABLE `enquiry` DISABLE KEYS */;
INSERT INTO `enquiry` VALUES (1,'Ivory Rios','201','881','cozafecaro@mailinator.com','Sapiente et amet hi',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-13 04:55:52','2021-04-13 04:55:52'),(2,'Harlan Conley','490','320','naqin@mailinator.com','Excepturi eum rerum',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-13 04:58:55','2021-04-13 04:58:55'),(3,'Martin Mclean','820','486','wyqyt@mailinator.com','Dolore laborum Cons',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-13 05:08:40','2021-04-13 05:08:40'),(4,'Moses Ramsey','356','354','rebyfij@mailinator.com','Animi id nemo velit',2,'MSCC1215E -CHARGE CONTROLLER 12V DC 15A CHARGING,10A LOAD','2021-04-13 05:09:39','2021-04-13 05:09:39'),(5,'Sybil Roman','566','705','gyri@mailinator.com','Laboris deserunt ven',2,'MSCC1215E -CHARGE CONTROLLER 12V DC 15A CHARGING,10A LOAD','2021-04-13 05:14:28','2021-04-13 05:14:28'),(6,'Sybil Roman','566','705','gyri@mailinator.com','Laboris deserunt ven',2,'MSCC1215E -CHARGE CONTROLLER 12V DC 15A CHARGING,10A LOAD','2021-04-13 05:16:09','2021-04-13 05:16:09'),(7,'Robert Wells','869','464','dote@mailinator.com','Dolor et iste eiusmo',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-14 08:15:13','2021-04-14 08:15:13'),(8,'Kiona Gomez','346','484','kinymu@mailinator.com','Consequuntur rerum a',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-14 08:16:32','2021-04-14 08:16:32'),(9,'Kiona Gomez','346','484','kinymu@mailinator.com','Consequuntur rerum a',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-14 08:17:01','2021-04-14 08:17:01'),(10,'xxxxxxxxxxxxxxxxxxxxxxxx','11111111111111','22222222222222','quxele@mailinator.com','Dignissimos lorem do',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-14 08:18:06','2021-04-14 08:18:06'),(11,'Shreyas Ganvkar xxxx','7899149177','22222222222222','shreyasganvkar2052@gmail.com','sdsdsdsdsdsdsdsdsdsd',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-14 08:22:24','2021-04-14 08:22:24'),(12,'Alexandra Mcintosh','576','652','japob@mailinator.com','Deserunt dolor labor',14,'MSDC2415D-CHARGE CONTROLLER 12/24V DC, AUTO DETECTION, 15A CHARGING,15A LOAD- DUSK TO DAWN DELUX','2021-04-16 08:28:53','2021-04-16 08:28:53'),(13,'Herrod Obrien','930','381','tugil@mailinator.com','Necessitatibus corru',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-16 08:29:24','2021-04-16 08:29:24'),(14,'Graham Suarez','1','859','mejuvyby@mailinator.com','Alias vel aperiam er',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-16 08:29:48','2021-04-16 08:29:48'),(15,'Ursula Donaldson','999','979','nako@mailinator.com','Consequatur Quasi r',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-16 08:30:38','2021-04-16 08:30:38'),(16,'Maxwell Carson','17','987','fafywyke@mailinator.com','Est minus recusandae',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-16 08:33:37','2021-04-16 08:33:37'),(17,'April Clements','531','874','xowunucyzu@mailinator.com','Aut placeat repelle',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-16 08:36:22','2021-04-16 08:36:22'),(18,'Charissa Flynn','170','622','rora@mailinator.com','Temporibus minim mag',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-18 23:45:24','2021-04-18 23:45:24'),(19,'Jackson Fuller','889','765','wanuvaw@mailinator.com','Esse cumque repudia',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-18 23:46:15','2021-04-18 23:46:15'),(20,'Jackson Fuller','889','765','wanuvaw@mailinator.com','Esse cumque repudia',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-18 23:46:36','2021-04-18 23:46:36'),(21,'Bethany Middleton','829','77','lopygyqom@mailinator.com','Corrupti laborum E',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-22 05:19:57','2021-04-22 05:19:57'),(22,'Hyatt Pittman','169','593','gabik@mailinator.com','Esse nihil magni au',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-22 05:33:04','2021-04-22 05:33:04'),(23,'Nevada Murphy','363','759','hahyta@mailinator.com','Tempora ducimus dis',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-22 05:38:04','2021-04-22 05:38:04'),(24,'Cassandra Lucas','733','286','tacawylite@mailinator.com','Nulla aliqua Elit',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-23 23:40:17','2021-04-23 23:40:17'),(25,'Cassandra Lucas','733','286','tacawylite@mailinator.com','Nulla aliqua Elit',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-23 23:41:17','2021-04-23 23:41:17'),(26,'erg','11111111111111','22222222222222','shreyasganvkar2052@gmail.com','ergg',1,'MSCC1205E-CHARGE CONTROLLER 12V DC 5A CHARGING,5A LOAD','2021-04-24 00:17:17','2021-04-24 00:17:17');
/*!40000 ALTER TABLE `enquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (17,'2021_04_09_153239_create_categories_table',1),(18,'2021_04_09_153602_create_subcategories_table',1),(19,'2021_04_09_153738_create_products_table',1),(20,'2021_04_09_153807_create_variants_table',1),(22,'2021_04_12_100153_create_enquiry_table',2),(23,'2021_05_05_055405_remove_pr_broch_from_products',3),(24,'2021_05_05_061931_add_var_img_to_variants',4),(25,'2021_05_06_045820_add_cy_img_to_categories',5),(27,'2021_05_06_052517_add_vr_ds_to_variants',6),(28,'2021_05_06_053137_remove_pr_ds_from_products',7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `PK_pr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pr_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pr_img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pr_man` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_sub_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PK_pr_id`),
  KEY `products_fk_sub_id_foreign` (`FK_sub_id`),
  CONSTRAINT `products_fk_sub_id_foreign` FOREIGN KEY (`FK_sub_id`) REFERENCES `subcategories` (`PK_sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'SOLAR CHARGE CONTROLLER RATINGS: 12/24V – 5/10/15A','This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control. Separate LED\'s clearly show the battery\'s charge level.','images/charge-c-03.jpg','pdf/User Manual charge controller_12_24V_5_10_15A.pdf',1,NULL,NULL),(2,'RATINGS: 12V – 20/30A SOLAR CHARGE CONTROLLER','This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control. Separate LED\'s clearly show the battery\'s charge level.','images/charge-c-01.jpg','pdf/User Manual charge controller_12_24V_5_10_15A.pdf',1,NULL,NULL),(3,'RATINGS: 12/24V – 20/30A SOLAR CHARGE CONTROLLER','This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control. Separate LED\'s clearly show the battery\'s charge level.','images/charge-c-02.jpg','pdf/User Manual charge controller_12_24V_5_10_15A.pdf',1,NULL,NULL),(4,'RATINGS: 12/24/48V – 40/50/60A SOLAR CHARGE CONTROLLER','This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control. Separate LED\'s clearly show the battery\'s charge level.','images/charge-c-03.jpg','pdf/User Manual charge controller_12_24V_5_10_15A.pdf',1,NULL,NULL),(5,'MAXIMUM POWER POINT TRACKING (MPPT- MOSFET VERSION)  RATINGS: 12/24/36/48/60/72/96-40/60A ','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium lacus laoreet ornare fermentum. Pellentesque in venenatis libero. Pellentesque non justo et nunc finibus varius id at orci. Suspendisse eget elit vehicula, dapibus libero vel, vulputate justo. In ac massa sed elit lacinia posuere. Mauris vel lacinia nulla.','images/charge-c-03.jpg','pdf/Tech spec-Priority Charge Controller_12_24_48V_20_30_40_50_60A.docx.pdf',2,NULL,NULL),(6,'MAXIMUM POWER POINT TRACKING (MPPT- IGBT VERSION)  RATINGS: 96/120/144/168/180/192/240/360 V- 40/60A','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium lacus laoreet ornare fermentum. Pellentesque in venenatis libero. Pellentesque non justo et nunc finibus varius id at orci. Suspendisse eget elit vehicula, dapibus libero vel, vulputate justo. In ac massa sed elit lacinia posuere. Mauris vel lacinia nulla.','images/charge-c-03.jpg','pdf/Tech spec-Priority Charge Controller_12_24_48V_20_30_40_50_60A.docx.pdf',2,NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subcategories` (
  `PK_sub_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_cy_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PK_sub_id`),
  KEY `subcategories_fk_cy_id_foreign` (`FK_cy_id`),
  CONSTRAINT `subcategories_fk_cy_id_foreign` FOREIGN KEY (`FK_cy_id`) REFERENCES `categories` (`PK_cy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategories`
--

LOCK TABLES `subcategories` WRITE;
/*!40000 ALTER TABLE `subcategories` DISABLE KEYS */;
INSERT INTO `subcategories` VALUES (1,'PWM','images/charge-c-01.jpg',1,NULL,NULL),(2,'MPPT','images/charge-c-02.jpg',1,NULL,NULL),(3,'Inverter','images/solar_inveter.jpg',2,NULL,NULL);
/*!40000 ALTER TABLE `subcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variants` (
  `PK_var_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `var_mod_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `var_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_pr_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `var_img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vr_ds` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vr_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`PK_var_id`),
  KEY `variants_fk_pr_id_foreign` (`FK_pr_id`),
  CONSTRAINT `variants_fk_pr_id_foreign` FOREIGN KEY (`FK_pr_id`) REFERENCES `products` (`PK_pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variants`
--

LOCK TABLES `variants` WRITE;
/*!40000 ALTER TABLE `variants` DISABLE KEYS */;
INSERT INTO `variants` VALUES (1,'MSCC1205E','CHARGE CONTROLLER 12V DC 5A \nCHARGING,5A LOAD',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(2,'MSCC1215E ','CHARGE CONTROLLER 12V DC \n15A CHARGING,10A LOAD',1,NULL,NULL,'images/charge-c-02.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(3,'MSCC1205ME','CHARGE CONTROLLER 12V DC 5A \nCHARGING,5A LOAD WITH \nMOBILE CHARGER',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(4,'MSCC1210ME','CHARGE CONTROLLER 12V DC 10A CHARGING,10A LOAD WITH MOBILE CHARGER',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(5,'MSCC1215ME','CHARGE CONTROLLER 12V DC 15A \nCHARGING,15A LOAD WITH MOBILE \nCHARGER',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(6,'MSCC245D','CHARGE CONTROLLER 12/24V DC, \nAUTO DETECTION, 5 A \nCHARGING,5A LOAD- DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(7,'MSCC2410D','CHARGE CONTROLLER 12/24V DC, \nAUTO DETECTION, 10A \nCHARGING,10A LOAD- DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(8,'MSCC2415D','CHARGE CONTROLLER 12/24V DC, AUTO DETECTION, 15A CHARGING,15A LOAD- DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(9,'MSDC125E','CHARGE CONTROLLER 12V DC 5A \nCHARGING,5A LOAD - DUSK TO \nDAWN',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(10,'MSDC1210E','CHARGE CONTROLLER 12V DC \n10A CHARGING,10A LOAD - DUSK \nTO DAWN',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(11,'MSDC1215E','CHARGE CONTROLLER 12V DC \n15A CHARGING,10A LOAD-DUSK \nTO DAWN',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(12,'MSDC2405D','CHARGE CONTROLLER 12/24V DC, \nAUTO DETECTION, 5A \nCHARGING,5A LOAD-DUSK TO \nDAWN,DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(13,' MSDC2410D','CHARGE CONTROLLER 12/24V DC, \nAUTO DETECTION, 10A \nCHARGING,10A LOAD- DUSK TO \nDAWN DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.'),(14,'MSDC2415D','CHARGE CONTROLLER 12/24V DC, \nAUTO DETECTION, 15A \nCHARGING,15A LOAD- DUSK TO \nDAWN DELUX',1,NULL,NULL,'images/charge-c-01.jpg','pdf/Technical specification-Charge Controller_12_24V_5_10_15A.pdf','The Charge Controller of Maruthi Solar product lines are the best option for compact, cost effective stand-alone solar systems. This charge controller’s centerpiece is a microprocessor/IC, which controls the security and information functions as well as guaranteeing optimal charge control. Maruthi Solar charge controller can also charge flooded /sealed lead acid/Lithium batteries. In order to maximize the battery’s life further, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.');
/*!40000 ALTER TABLE `variants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-08 17:09:11
